package com.tendai.mobilebanking;

import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tendai.mobilebanking.Utilities.LogOutListener;
import com.tendai.mobilebanking.recyclerView.AccountDetailsAdapter;
import com.tendai.mobilebanking.recyclerView.AccountDetailsItem;

import java.util.ArrayList;

public class AccountDetailsActivity extends BaseActivity implements LogOutListener {

    private ArrayList<AccountDetailsItem> mItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_details);

        setTitle("Account Details");
        createMainList();
        buildRecyclerView();
    }

    private void buildRecyclerView() {
        RecyclerView mRecyclerView = findViewById(R.id.recyclerView);
        AccountDetailsAdapter mAdapter = new AccountDetailsAdapter(mItems);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);


        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void createMainList() {
        mItems = new ArrayList<>();
        double myBalance = application.getmAuth().getUser().getBalance();
        String balance = String.valueOf(myBalance);

        mItems.add(new AccountDetailsItem("Account Name", "Tendai Musakanye"));
        mItems.add(new AccountDetailsItem("Account Number", "100****695"));
        mItems.add(new AccountDetailsItem("Branch", "Harare"));
        mItems.add(new AccountDetailsItem("Mobile Number", application.getmAuth().getUser().getPhoneNumber()));
        mItems.add(new AccountDetailsItem("Available Balance", balance));
    }

    @Override
    public void doLogOut() {
        super.doLogOut();
        finish();
    }

}

