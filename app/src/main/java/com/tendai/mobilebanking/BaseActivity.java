package com.tendai.mobilebanking;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.tendai.mobilebanking.Utilities.LogOutListener;
import com.tendai.mobilebanking.Utilities.LogOutTimeUtil;
import com.tendai.mobilebanking.database.Statement;
import com.tendai.mobilebanking.database.StatementViewModel;
import com.tendai.mobilebanking.infrastructure.MobileBanking;

import java.util.List;

public  abstract  class BaseActivity extends AppCompatActivity implements LogOutListener {

    public MobileBanking application;
    public StatementViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        application = (MobileBanking) getApplication();
        viewModel = ViewModelProviders.of(this).get(StatementViewModel.class);
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.e("BaseActivity: ", "onPause");
        LogOutTimeUtil.startLogOutTimer(this, this);
    }

    // todo  if user is not interacting, close the app as well.
    // todo: create an async task or use the existing one.

    @Override
    protected void onStop() {
        super.onStop();

        Log.e("BaseActivity: ", "OnStop");
        LogOutTimeUtil.startLogOutTimer(this, this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        Log.e("BaseActivity: ", "onRestart");
        LogOutTimeUtil.stopLogOutTimer();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.e("BaseActivity: ", "onResume");
        LogOutTimeUtil.stopLogOutTimer();
    }

    @Override
    public void doLogOut() {
        application.getmAuth().getUser().setIsLoggedIn(false);
    }




}
