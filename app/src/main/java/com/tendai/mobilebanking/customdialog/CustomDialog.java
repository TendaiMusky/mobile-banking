package com.tendai.mobilebanking.customdialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import com.tendai.mobilebanking.R;

public class CustomDialog {
    public static CustomDialog sInstance;
    public Dialog dialog;

    public CustomDialog(){

    }

    public static CustomDialog getInstance(){
        if (sInstance == null) {
            sInstance = new CustomDialog();
        }
        return sInstance;
    }

    public void showProgressBar(Context context){
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_things);
        dialog.findViewById(R.id.progressBar);
        dialog.findViewById(R.id.progressBarText);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void hideProgressBar(){
        if (dialog != null) {
            dialog.dismiss();
            dialog= null;
        }
    }
}
