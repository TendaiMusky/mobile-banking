package com.tendai.mobilebanking.notifications;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.tendai.mobilebanking.R;


public class NotificationUtils extends ContextWrapper {
    private static final String CHANNEL_ONE_ID = "com.tendai.mobilebanking.notifications.CHANNEL_ONE_ID";
    private static final String CHANNEL_ONE_NAME = "com.tendai.mobilebanking.notifications.CHANNEL_ONE_NAME";


    // bad practice though to make fields public
    public static String state;
    public static  int notifyId;
    private NotificationManager notificationManager;

    public NotificationUtils(Context context){
        super(context);
        createNotificationChannel();
    }

    private void createNotificationChannel(){
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
            NotificationChannel channelOne = new NotificationChannel(CHANNEL_ONE_ID,CHANNEL_ONE_NAME,NotificationManager.IMPORTANCE_HIGH);
            channelOne.enableLights(true);
            channelOne.enableVibration(true);
            channelOne.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            channelOne.setLightColor(Color.GRAY);
            channelOne.setShowBadge(true);
            getNotificationManager().createNotificationChannel(channelOne);
        }
    }

    public NotificationManager getNotificationManager(){
        if (notificationManager == null) {
            notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notificationManager;
    }

    public NotificationCompat.Builder transactionSuccessNotification(){
        Intent intent = new Intent(this,MarkAsRead.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this,0,intent,0);

        return new NotificationCompat.Builder(this,CHANNEL_ONE_ID)
                .setSmallIcon(R.drawable.ic_account_balance_black_24dp)
                .setAutoCancel(false)
                .setContentTitle("Transaction Successful")
                .setWhen(System.currentTimeMillis())
//                .addAction(R.drawable.ic_account_balance_black_24dp,"Mark As Read",pendingIntent)
                .setStyle(new NotificationCompat.BigTextStyle()
                .bigText(state));
        // todo add a mark as read feature



    }
}
