package com.tendai.mobilebanking.notifications;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class MarkAsRead extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        int id = NotificationUtils.notifyId;
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(id);

    }

}
