package com.tendai.mobilebanking;

import android.accounts.Account;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.tendai.mobilebanking.Utilities.LogOutListener;
import com.tendai.mobilebanking.database.StatementActivity;

public class MainActivity extends BaseActivity implements View.OnClickListener, LogOutListener {

    Button accountDetails;
    Button transferMoney;
    Button statement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("MyPrefs", 0);
//        int b = application.getmAuth().getUser().getBalance();
//        int balance = sharedPreferences.getInt("balance",b);
//        application.getmAuth().getUser().setBalance(balance);

        // TODO: lock app in portrait mode
        //Todo: implement a tabbed layout
        //todo: delete sharedPreferences
        // todo: implement a tabbed a layout and isolate the miniStatement functionality

        accountDetails = findViewById(R.id.fragment_home_button1);
        transferMoney = findViewById(R.id.fragment_home_button2);
        statement = findViewById(R.id.button_miniStatement);

        accountDetails.setOnClickListener(this);
        transferMoney.setOnClickListener(this);
        statement.setOnClickListener(this);

        if (!application.getmAuth().getUser().getIsLoggedIn()) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }

        setTitle("Home");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (!application.getmAuth().getUser().getIsLoggedIn()) {
            startActivity(new Intent(this,LoginActivity.class));
            finish();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("MyPrefs", 0);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putInt("balance", application.getmAuth().getUser().getBalance());
//        editor.apply();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        if (id == R.id.fragment_home_button1) {
            Intent intent = new Intent(this, AccountDetailsActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

        }
        if (id == R.id.fragment_home_button2) {
            Intent intent = new Intent(this,TransferMoneyActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);


        }if (id == R.id.button_miniStatement){
            Intent intent = new Intent(this,StatementActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

        }

    }

    @Override
    public void doLogOut() {
        super.doLogOut();
        finish();
    }
}
