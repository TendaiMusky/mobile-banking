package com.tendai.mobilebanking.database;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.tendai.mobilebanking.R;

public class StatementAdapter extends ListAdapter<Statement, StatementAdapter.StatementHolder> {

    public  StatementAdapter(){
        super(DIFF_CALLBACK);
    }

    @NonNull
    @Override
    public StatementHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_statement,parent,false);

        return new StatementHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StatementHolder holder, int position) {
        Statement currentStatement = getItem(position);

        holder.dateOnly.setText(currentStatement.getDateOnly());
        holder.statement.setText(currentStatement.getState());
        holder.timeOnly.setText(currentStatement.getTimeOnly());
    }

    public Statement getNoteAt(int position){
        return getItem(position);
    }

    class StatementHolder extends RecyclerView.ViewHolder{
        private TextView dateOnly;
        private TextView timeOnly;
        private TextView statement;

        public StatementHolder(@NonNull View itemView) {
            super(itemView);

            dateOnly = itemView.findViewById(R.id.textView_date_only);
            timeOnly = itemView.findViewById(R.id.textView_time_only);
            statement = itemView.findViewById(R.id.textView_statement);
        }
    }

    private static final DiffUtil.ItemCallback<Statement> DIFF_CALLBACK = new DiffUtil.ItemCallback<Statement>() {
        @Override
        public boolean areItemsTheSame(@NonNull Statement oldItem, @NonNull Statement newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Statement oldItem, @NonNull Statement newItem) {
            return oldItem.getDateOnly().equals(newItem.getDateOnly())&&
                    oldItem.getState().equals(newItem.getState())&&
                    oldItem.getTimeOnly().equals(newItem.getTimeOnly());
        }
    };
}
