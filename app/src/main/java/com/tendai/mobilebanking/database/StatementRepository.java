package com.tendai.mobilebanking.database;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class StatementRepository {

    private StatementDao statementDao;
    private LiveData<List<Statement>> allStatements;

    public StatementRepository(Application application){
        StatementDatabase database = StatementDatabase.getInstance(application);
        statementDao = database.statementDao();
        allStatements = statementDao.getAllStatements();
    }

    void insert(Statement statement){
        new InsertStatementAsyncTask(statementDao).execute(statement);
    }

    void delete(Statement statement){
        new DeleteStatementAsyncTask(statementDao).execute(statement);
    }

    public LiveData<List<Statement>>getAllStatements(){
        return allStatements;
    }

    private static class InsertStatementAsyncTask extends AsyncTask<Statement,Void,Void>{

        StatementDao statementDao;

        private InsertStatementAsyncTask(StatementDao dao){
            this.statementDao = dao;

        }

        @Override
        protected Void doInBackground(Statement... statements) {

            statementDao.insertStatement(statements[0]);
            return null;
        }
    }

    private static class DeleteStatementAsyncTask extends AsyncTask<Statement,Void,Void>{

        StatementDao statementDao;

        private DeleteStatementAsyncTask(StatementDao dao){
            this.statementDao = dao;

        }

        @Override
        protected Void doInBackground(Statement... statements) {
            statementDao.deleteStatement(statements[0]);
            return null;
        }
    }
}
