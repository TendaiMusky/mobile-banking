package com.tendai.mobilebanking.database;


import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "statement_table")
public class Statement {


    @PrimaryKey(autoGenerate = true)
    private int id;

    private String state;

    private String dateOnly;
    private String timeOnly;

    public Statement( String dateOnly,String state, String timeOnly){
        this.state = state;
        this.dateOnly = dateOnly;
        this.timeOnly = timeOnly;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getState() {
        return state;
    }

    public String getDateOnly() {
        return dateOnly;
    }

    public String getTimeOnly() {
        return timeOnly;
    }



}
