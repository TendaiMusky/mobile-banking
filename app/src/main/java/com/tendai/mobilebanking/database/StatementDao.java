package com.tendai.mobilebanking.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface StatementDao {

    @Insert
    void insertStatement(Statement statement);

    @Delete
    void deleteStatement(Statement statement);

    // Not sure what the live data does yet

    @Query("SELECT * FROM statement_table")
    LiveData<List<Statement>> getAllStatements();
}
