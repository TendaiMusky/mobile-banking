package com.tendai.mobilebanking.database;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class StatementViewModel extends AndroidViewModel {
    StatementRepository repository;
    LiveData<List<Statement>> allStatements;

    public StatementViewModel(@NonNull Application application){
        super(application);

        repository = new StatementRepository(application);
        allStatements = repository.getAllStatements();
    }

    public void insert(Statement statement){
        repository.insert(statement);
    }

    public void delete(Statement statement){
        repository.delete(statement);
    }

    public LiveData<List<Statement>> getAllStatements(){
        return allStatements;
    }

}
