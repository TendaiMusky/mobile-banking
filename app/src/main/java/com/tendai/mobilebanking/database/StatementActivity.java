package com.tendai.mobilebanking.database;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tendai.mobilebanking.BaseActivity;
import com.tendai.mobilebanking.MainActivity;
import com.tendai.mobilebanking.R;
import com.tendai.mobilebanking.Utilities.LogOutListener;

import java.util.List;

public class StatementActivity extends BaseActivity implements LogOutListener {

    private StatementAdapter statementAdapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statement);

        setTitle("Statement");
        recyclerView = findViewById(R.id.statement_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        statementAdapter = new StatementAdapter();
        recyclerView.setAdapter(statementAdapter);


        viewModel.getAllStatements().observe(this, new Observer<List<Statement>>() {
            @Override
            public void onChanged(List<Statement> statements) {
                // Not sure why I am doing this entirely
                statementAdapter.submitList(statements);
            }
        });

        deleteOnSwipe();

    }

    private void deleteOnSwipe(){
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                AlertDialog.Builder builder = new AlertDialog.Builder(StatementActivity.this)
                        .setMessage("Delete Statement")
                        .setCancelable(true)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                viewModel.delete(statementAdapter.getNoteAt(viewHolder.getAdapterPosition()));

                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                    statementAdapter.notifyItemChanged(viewHolder.getAdapterPosition());
                            }
                        });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();

            }
        };

        ItemTouchHelper it = new ItemTouchHelper(simpleCallback);
        it.attachToRecyclerView(recyclerView);
    }

    @Override
    public void doLogOut() {
        super.doLogOut();
        finish();
    }

}
