package com.tendai.mobilebanking.Utilities;

import android.app.ActivityManager;
import android.content.Context;
import android.os.AsyncTask;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

public class LogOutTimeUtil {
    private static Timer sTimer;
    private static final int LOGOUT_TIME = 20 * 1000;

    public static synchronized void startLogOutTimer(final Context context, final LogOutListener logOutListener) {
        if (sTimer != null) {
            sTimer.cancel();
            sTimer = null;
        }

        if (sTimer == null) {
            sTimer = new Timer();
        }

        sTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                cancel();
                sTimer = null;

                try {
                    boolean isInBackground = new BackgroundCheckTask().execute(context).get();
                    if (isInBackground) {
                        logOutListener.doLogOut();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }, LOGOUT_TIME);
    }

    public static synchronized void stopLogOutTimer() {
        if (sTimer != null) {
            sTimer.cancel();
            sTimer = null;
        }
    }

    static class BackgroundCheckTask extends AsyncTask<Context, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Context... contexts) {
            final Context context = contexts[0].getApplicationContext();
            return isAppBackground(context);
        }

        private boolean isAppBackground(Context context) {
            final String packageName = context.getPackageName();

            ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> appProcessInfos = activityManager.getRunningAppProcesses();

            if (appProcessInfos == null) {
                return false;
            }

            for (ActivityManager.RunningAppProcessInfo processInfo : appProcessInfos) {

                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_CACHED &&
                        processInfo.processName.equals(packageName)) {
                    return true;
                }
//                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND &&
//                        processInfo.processName.equals(packageName)) {
//                    return true;
//                }
            }
            return false;
        }
    }
}
