package com.tendai.mobilebanking;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.biometric.BiometricPrompt;
import androidx.fragment.app.DialogFragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.tendai.mobilebanking.Utilities.LogOutListener;
import com.tendai.mobilebanking.customdialog.CustomDialog;

import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

public class LoginActivity extends BaseActivity implements LogOutListener, View.OnClickListener, AdapterView.OnItemSelectedListener {
    public static final String TAG = "LoginActivity";

    FirebaseAuth firebaseAuth;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks;
    EditText phoneNumber;
    Spinner codePicker;


    private Handler handler = new Handler();
    private Executor mExecutor = new Executor() {
        @Override
        public void execute(Runnable command) {
            handler.post(command);
        }
    };
    private CustomDialog customDialog;
    private String vCode;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

//        SharedPreferences sharedPreferences = getSharedPreferences("MyPrefs", 0);
//        sharedPreferences.edit().remove("counterValue").apply();


        getPrefs();

        customDialog = CustomDialog.getInstance();
        Button loginButton = findViewById(R.id.fragment_login_button_login);
        phoneNumber = findViewById(R.id.activity_login_phoneNumber);
        codePicker = findViewById(R.id.countryCodeSpinner);

        populateSpinner();


        codePicker.setOnItemSelectedListener(this);
        loginButton.setOnClickListener(this);
    }

    @Override
    public void doLogOut() {
        super.doLogOut();
        finish();
    }

    // todo: see what a bank statement really looks like you know.

    @Override
    public void onClick(View v) {
        if (!validateNumber()) {
            Log.e(getClass().getSimpleName(), "Validate");
        } else {
            if (application.getmAuth().getUser().getCounter() == 0) {
                verifyPhoneNumberDialog();
            } else {

//                application.getmAuth().getUser().setIsLoggedIn(true);
//                startActivity(new Intent(this,MainActivity.class));
//                finish();
                showBiometricPrompt();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        customDialog.hideProgressBar();

        // todo: handle the firebase authentication callbacks.
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("Loginact: ", "onStop");
        finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        parent.getItemAtPosition(position);

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void getPrefs() {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("MyPrefs", 0);
        int counter = sharedPreferences.getInt("counterValue", 0);
        application.getmAuth().getUser().setCounter(counter);

    }

    private void startPrefs() {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("MyPrefs", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("counterValue", application.getmAuth().getUser().getCounter());
        editor.apply();
    }

    private boolean validateNumber() {
        String x = phoneNumber.getText().toString().trim();
        application.getmAuth().getUser().setPhoneNumber("0" + x);
        if (x.isEmpty()) {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this)
                    .setTitle("Field Empty")
                    .setMessage("Please enter a phone number")
                    .setPositiveButton("Ok", null);
            // todo create a custon Dialog fragment maybe or maybe not.

            android.app.AlertDialog alertDialog = builder.create();
            alertDialog.show();

            return false;
        } else if (x.length() < 9) {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this)
                    .setTitle("Invalid Phone Number")
                    .setMessage("Phone number is too short." +
                            " Please enter a valid phone number")
                    .setPositiveButton("Ok", null);
            // todo create a custon Dialog fragment maybe or maybe not.

            android.app.AlertDialog alertDialog = builder.create();
            alertDialog.show();

            return false;

        } else if (x.length() > 9) {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this)
                    .setTitle("Invalid Phone Number.")
                    .setMessage("Phone number is too long.\n" +
                            "Please enter a valid phone number.")
                    .setPositiveButton("Ok", null);

            android.app.AlertDialog alertDialog = builder.create();
            alertDialog.show();

            return false;
        } else {
            return true;
        }

    }

    private void populateSpinner() {
        ArrayAdapter<CharSequence> codes = ArrayAdapter.createFromResource(this,
                R.array.country_codes,
                android.R.layout.simple_spinner_item);

        codes.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        codePicker.setAdapter(codes);
    }

    private void showBiometricPrompt() {
        BiometricPrompt.PromptInfo promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setConfirmationRequired(false)
                .setTitle("Use biometric to login")
                .setNegativeButtonText("Cancel")
                .build();

        final BiometricPrompt biometricPrompt = new BiometricPrompt(LoginActivity.this, mExecutor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);



            }

            @Override
            public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);

                application.getmAuth().getUser().setIsLoggedIn(true);
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                Toast.makeText(LoginActivity.this, "Login Success", Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();


            }
        });

        biometricPrompt.authenticate(promptInfo);
    }

    private void verifyPhoneNumberDialog() {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this)
                .setTitle("Phone Verification")
                .setMessage("Please verify your phone number to continue")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        customDialog.showProgressBar(LoginActivity.this);
                        initializeFirebaseVerification();
                        getOtp();
                    }
                });

        AlertDialog alertDialog = alertBuilder.create();
        alertDialog.show();

    }

    private void initializeFirebaseVerification() {

        firebaseAuth = FirebaseAuth.getInstance();

        callbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                Log.e(getClass().getSimpleName(), " onVerificationCompleted");
                signInWithPhone(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {

                Log.e(getClass().getSimpleName(), " onVerificationFailed");
                Log.e(TAG, "onVerificationFailed", e);

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    Log.e(TAG, "Invalid Request");
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    Log.e(TAG, "Too many sms");
                }

                customDialog.hideProgressBar();
                Toast.makeText(LoginActivity.this, "Verification Failed", Toast.LENGTH_SHORT).show();
                // FindSome Interactive way to notify the use the verification failed.
            }

            @Override
            public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);

                Log.e(getClass().getSimpleName(), " onCodeSent");
                vCode = s;


            }

            @Override
            public void onCodeAutoRetrievalTimeOut(@NonNull String s) {
                super.onCodeAutoRetrievalTimeOut(s);
                Log.e(getClass().getSimpleName(), " onCodeAutoRetrievalTimeOut");
                customDialog.hideProgressBar();

                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this)
                        .setTitle("Verification Error")
                        .setMessage("Please make sure that your sim card is in your phone")
                        .setPositiveButton("Ok",null);

                AlertDialog dialog = builder.create();
                dialog.show();


            }
        };

    }

    private void getOtp() {
        Log.e(getClass().getSimpleName(), " getOtp");
        String number = "+263" + phoneNumber.getText().toString();

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,
                60,
                TimeUnit.SECONDS,
                LoginActivity.this,
                callbacks
        );
    }

    private void signInWithPhone(PhoneAuthCredential phoneAuthCredential) {

        Log.e(getClass().getSimpleName(), " signInWithPhone");
        firebaseAuth.signInWithCredential(phoneAuthCredential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            Log.e(getClass().getSimpleName(), " Task Successful");

                            application.getmAuth().getUser().setCounter(1);
                            startPrefs();
                            customDialog.hideProgressBar();
                            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this)
                                    .setTitle("Verification Success")
                                    .setMessage("Phone Number Verified. Please Login")
                                    .setPositiveButton("Ok", null);

                            AlertDialog alertDialog = builder.create();
                            alertDialog.show();


                        } else {
                            Log.e(getClass().getSimpleName(), " taskFailed");
                            // Sign in failed, display a message and update the UI

                            Log.w(TAG, "signInWithCredential:failure", task.getException());

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                            }

                            customDialog.hideProgressBar();
                            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this)
//                                    .setTitle("Verification Failed")
                                    .setMessage("Phone Number Not Verified")
                                    .setIcon(R.drawable.ic_error)
                                    .setPositiveButton("Ok", null);

                            AlertDialog alertDialog = builder.create();
                            alertDialog.show();

                        }
                    }
                });


    }
}
