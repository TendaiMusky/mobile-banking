package com.tendai.mobilebanking.recyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tendai.mobilebanking.R;

import java.util.ArrayList;

public class AccountDetailsAdapter extends RecyclerView.Adapter<AccountDetailsAdapter.AccountDetailsViewHolder> {
    ArrayList<AccountDetailsItem> items;

    public AccountDetailsAdapter(ArrayList<AccountDetailsItem> list){
        this.items = list;
    }

    @NonNull
    @Override
    public AccountDetailsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_account_details,parent,false);
        AccountDetailsViewHolder viewHolder = new AccountDetailsViewHolder(view);

        return  viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AccountDetailsViewHolder holder, int position) {
        AccountDetailsItem accountDetailsItem = items.get(position);

        holder.label.setText(accountDetailsItem.getLabel());
        holder.content.setText(accountDetailsItem.getContent());

    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    static class AccountDetailsViewHolder extends RecyclerView.ViewHolder {
        TextView label;
        TextView content;

        public AccountDetailsViewHolder(@NonNull View itemView) {
            super(itemView);

            label = itemView.findViewById(R.id.textView_label);
            content = itemView.findViewById(R.id.textView_content);
        }
    }


}
