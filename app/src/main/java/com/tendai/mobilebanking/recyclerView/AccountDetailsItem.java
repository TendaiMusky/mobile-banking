package com.tendai.mobilebanking.recyclerView;

public class AccountDetailsItem {

    private String label;
    private String content;

    public AccountDetailsItem(String label, String content){
        this.label = label;
        this.content = content;
    }

    public String getLabel() {
        return label;
    }

    public String getContent() {
        return content;
    }

}
