package com.tendai.mobilebanking;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricPrompt;
import androidx.core.app.NotificationCompat;

import com.google.android.material.textfield.TextInputLayout;
import com.tendai.mobilebanking.Utilities.LogOutListener;
import com.tendai.mobilebanking.Utilities.LogOutTimeUtil;
import com.tendai.mobilebanking.database.Statement;
import com.tendai.mobilebanking.notifications.NotificationUtils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executor;

public class TransferMoneyActivity extends BaseActivity implements  LogOutListener,View.OnClickListener {

    @ColorInt public static final int ACCENT = 0xD81B60;


    TextInputLayout accountNumber;
    Spinner bankName;
    TextInputLayout mobileNumber;

    TextInputLayout amount;
    Button transferMoney;
    NotificationUtils notificationUtils;
    NotificationCompat.Builder builder;

    private String amountToTransfer;
    private Handler handler = new Handler();
    private Executor mExecutor = new Executor() {
        @Override
        public void execute(Runnable command) {
            handler.post(command);
        }
    };
    private ColorStateList colorStateList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_money);

        setTitle("Transfer Money");

        builder = null;
        notificationUtils = new NotificationUtils(this);
        colorStateList = ColorStateList.valueOf(Color.RED);

        accountNumber = findViewById(R.id.transfer_money_account_number_textInputLayout);
        amount = findViewById(R.id.textInputLayout_amount);
        mobileNumber = findViewById(R.id.mobile_number_textInputLayout);
        transferMoney = findViewById(R.id.button_transfer_money);
        bankName = findViewById(R.id.transfer_money_bank_spinner);

        accountNumber.getEditText().addTextChangedListener(textWatcher);
        amount.getEditText().addTextChangedListener(textWatcher);
        mobileNumber.getEditText().addTextChangedListener(textWatcher);

        populateBankSpinner();
        transferMoney.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
//        LogOutTimeUtil.startLogOutTimer(this,this);
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        Log.e(getClass().getSimpleName(),"onUserInteracted");
//        LogOutTimeUtil.stopLogOutTimer();

    }

    @Override
    public void onClick(View v) {
        if (!validateAccountNumber() | !validateAmount() | !validatePhoneNumber()) {
            Log.e(getClass().getSimpleName(),"Validate");
        }else{
            showBiometricPrompt();
        }

    }

    @Override
    public void doLogOut() {
        super.doLogOut();
        finish();
    }

    private void populateBankSpinner() {
        ArrayAdapter<CharSequence> banks = ArrayAdapter.createFromResource(this,
                R.array.banks,
                android.R.layout.simple_spinner_item
        );

        banks.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        bankName.setAdapter(banks);
    }

    private void showBiometricPrompt() {
        BiometricPrompt.PromptInfo promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setConfirmationRequired(false)
                .setTitle("Confirm Payment")
                .setNegativeButtonText("Cancel")
                .build();

        final BiometricPrompt biometricPrompt = new BiometricPrompt(TransferMoneyActivity.this, mExecutor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                //todo: onVerificationComplete send an email.
            }

            @Override
            public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                //todo: update the database
                // todo: send a notification

                double amountTransferred = Double.parseDouble(amountToTransfer);
                application.getmAuth().getUser().setSentAmount(amountTransferred);
                double myBalance = application.getmAuth().getUser().getBalance();
                double newBal = myBalance - amountTransferred;
                application.getmAuth().getUser().setBalance(newBal);


                insert();
                int id = application.getmAuth().getUser().getNotificationId();
                NotificationUtils.notifyId = id;
                postNotification(id);
                finish();
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();

            }
        });

        biometricPrompt.authenticate(promptInfo);
    }

    private void postNotification(int id) {
        builder = notificationUtils.transactionSuccessNotification();
        notificationUtils.getNotificationManager().notify(id, builder.build());
    }

    private boolean validateAccountNumber() {
        String accNums = accountNumber.getEditText().getText().toString().trim();


        accountNumber.setErrorTextColor(colorStateList);
        if (accNums.isEmpty()) {
            accountNumber.setError("Account Number cannot be empty");
            return false;

        } else if (accNums.length() > 15) {

            accountNumber.setError("Account Number is too long");
            return false;

        } else if (accNums.length() < 10) {
            accountNumber.setError("Account number is too small\nIt must have at least 10 characters");

            return false;
        } else {
            accountNumber.setError(null);
            return true;
        }
    }

    private boolean validatePhoneNumber() {
        String mobileNums = mobileNumber.getEditText().getText().toString().trim();

        mobileNumber.setErrorTextColor(colorStateList);
        if (mobileNums.isEmpty()) {
            mobileNumber.setError("Please enter phone number");

            return false;

        } else if (mobileNums.length() > 10) {
            mobileNumber.setError("Phone number is too long");

            return false;

        } else if (mobileNums.length()<10) {
            mobileNumber.setError("Phone number is too small");

            return false;

        } else if (!Patterns.PHONE.matcher(mobileNums).matches()) {
            mobileNumber.setError("Please enter a valid mobile number");

            return false;
        } else {
            mobileNumber.setError(null);
            return true;
        }
    }

    private boolean validateAmount() {
        amountToTransfer = amount.getEditText().getText().toString().trim();
        double availableBalance = application.getmAuth().getUser().getBalance();

        amount.setErrorTextColor(colorStateList);

        if (amountToTransfer.isEmpty()) {
            amount.setError("Please enter an amount");

            return false;
        }

        double aTT = Double.parseDouble(amountToTransfer);
        if (aTT > availableBalance) {

                amount.setError("Amount exceeds available balance");

            return false;

        } else {
            amount.setError(null);
            return true;
        }


    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            accountNumber.setError(null);
            mobileNumber.setError(null);
            amount.setError(null);

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    protected void insert() {
        double amountSent = application.getmAuth().getUser().getSentAmount();
        double newBalance = application.getmAuth().getUser().getBalance();

        DecimalFormat df = new DecimalFormat("###.##");
        String newB = df.format(newBalance);

        String accNums = application.getmAuth().getUser().getAccNums();
        NotificationUtils.state = "A transaction of USD " + amountSent +
                " was processed on A/C " + accNums + ". Your new account balance is USD " +
                newB;

        Date time = new Date();
        SimpleDateFormat timeFormat;
        timeFormat = new SimpleDateFormat("HH:mm");
        String t = timeFormat.format(time);

        Date date = new Date();
        SimpleDateFormat dateFormat;
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat.applyPattern("EEE, d MMM yyyy");
        String d = dateFormat.format(date);

        Statement statement = new Statement(d, NotificationUtils.state, t);
        viewModel.insert(statement);

        //todo: can't sent balance above balance in phone dialog.
    }

}
