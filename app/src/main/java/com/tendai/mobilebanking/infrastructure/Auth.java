package com.tendai.mobilebanking.infrastructure;

import android.content.Context;

public class Auth{
    private final Context mContext;
    private User user;


    public Auth(Context mContext) {
        this.mContext = mContext;
        user = new User();
    }

    public User getUser() {
        return user;
    }
}
