package com.tendai.mobilebanking.infrastructure;

import android.app.Application;

public class MobileBanking extends Application {
    private Auth mAuth;

    @Override
    public void onCreate() {
        super.onCreate();
        mAuth = new Auth(this);
    }

    public Auth getmAuth() {
        return mAuth;
    }
}
