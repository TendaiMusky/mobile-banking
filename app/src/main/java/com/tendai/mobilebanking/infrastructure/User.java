package com.tendai.mobilebanking.infrastructure;

public class User {
    private boolean mIsLoggedIn;

    private int mCounter = 0;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    private String phoneNumber;

    public int getNotificationId() {
        for(int i = 0;i<50;i++){
            notificationId = (int)(Math.random()*50);
        }
        return notificationId;
    }

    private int notificationId;

    public String getAccNums() {
        return accNums;
    }

    private String accNums = "1008011695";

    private double balance = 50000.00;

    private double sentAmount = 0.00;

    private String mobileNumber = "774446819";

    public int getCounter() {
        return mCounter;
    }

    public void setCounter(int counter) {
        this.mCounter = counter;
    }

    public boolean getIsLoggedIn() {
        return mIsLoggedIn;
    }

    public void setIsLoggedIn(boolean mIsLoggedIn) {
        this.mIsLoggedIn = mIsLoggedIn;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getSentAmount() {
        return sentAmount;
    }

    public void setSentAmount(double sentAmount) {
        this.sentAmount = sentAmount;
    }


}
